@extends('store.template')

@section('content')
<div class="container text-center">
	<div class="page-header">
	  <h1><i class="fa fa-shopping-cart"></i> Detalle del producto</h1>
	</div>

	<div class="row">
		<div class="col-md-6">
			<div class="product-block">
				<img src="{{ url($product->image) }}" width="200" height="200">
			</div>	
		</div>
		<div class="col-md-6">
			<div class="product-block">
				<h3>{{ $product->name }}</h3><span style="color: grey;">Precio: Bsf. {{ number_format($product->price) }}</span><hr>
				<div class="product-info panel">
					<h3>{{ $product->description }}</h3>
<br><hr>
					<p>
						<a class="btn btn-success btn-block" href="{{ route('cart-add', $product->slug) }}">
							La quiero <i class="fa fa-cart-plus fa-2x"></i>
						</a>
					</p>
				</div>
			</div>	
		</div>
	</div><hr>

	<p>
		<a class="btn btn-primary" href="{{ route('home') }}">
			<i class="fa fa-chevron-circle-left"></i> Regresar
		</a>
	</p>
</div>
@stop