      <div class="container">
        <hr class="star-dark mb-5">
        <div class="row text-center">

              @foreach($products->take(4) as $product)
@if ($product->category_id==$categor->id)
          <div class="col-md-3 col-lg-3">
      <div class="product whites-paneli">
        <h3>{{ $product->name }}</h3><hr>
        <img src="{{ $product->image }}" height="150" width="200">
        <div class="product-info panel">
          <h4>{{ $product->extract }}</h4>
          <h3><span class="label label-success">Precio: Bsf. {{ number_format($product->price) }}</span></h3>
          <p>
            <a class="btn btn-primary" href="{{ route('cart-add', $product->slug) }}">
              <i class="fa fa-cart-plus" style="font-size: 24px;"></i> La quiero
            </a>
            <a class="btn btn-info" href="{{ route('product-detail', $product->slug) }}"><i class="fa fa-chevron-circle-right" style="font-size: 24px;"></i> Leer mas</a>
          </p>
        </div>
      </div>
          </div>
              @endif
              @endforeach
        </div>
      </div>
      <br><br>