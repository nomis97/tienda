        @extends('store.partials.nava')
        <br><br><br><br>

    <section class="portfolio" id="portfolio">
      @if(count($products))
              @foreach($products->take(1) as $product)
        <h4 class="text-center text-uppercase text-secondary mb-0">{{$product->category->name}}</h4>
                      @endforeach

        <hr class="star-dark mb-5">
      <div class="container">
        <div class="row text-left">

              @foreach($products as $product)

          <div class="col-md-3 col-lg-11">
      <div class="product ">
        <hr>
        <h3 class="text-center">{{ $product->name }}</h3><hr>
          <span class="text-right"><h3 style="color: grey;">Precio: Bsf. {{ number_format($product->price) }}</h3></span>
        <img src="{{ url($product->image) }}" height="150" width="200"> <span class="sty">{{ $product->extract }}</span>
        <div class="product-info panel">
         <span class="text-right"> <p>
            <a class="btn btn-primary" href="{{ route('cart-add', $product->slug) }}">
              <i class="fa fa-cart-plus" style="font-size: 24px;"></i> La quiero
            </a>
            <a class="btn btn-info" href="{{ route('product-detail', $product->slug) }}"><i class="fa fa-chevron-circle-right" style="font-size: 24px;"></i> Leer mas</a>
          </p></span>
        </div>
      </div>
          </div>
              @endforeach
        </div>
      </div>
      @else
        <h3 class="text-center"><span class="label label-warning">No se encontro ningun producto :(</span></h3>
      @endif
    </section>
            <style type="text/css">

          span.sty{
            color: #333;
            font-size: 15px; 
            margin-left: 30px;
            font-family: "Proxima Nova",Helvetica,Arial,sans-serif;
          }
            </style>
       