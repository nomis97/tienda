       @include('libreri')
         <body id="page-top">


    <nav class="navbar navbar-expand-lg bg-secondary fixed-top text-uppercase" id="mainNav">
        <a class="navbar-brand js-scroll-trigger" href="/Tiendita">CO-Tienda virtual</a>
      <div class="container">
                <div class="row col-md-5">
<div class="col-md-12">
{!! Form::open(['route' => 'seccione', 'method' => 'GET', 'role' => 'search']) !!}
    <div class="input-group">
      {!! Form::text('name', null, ['class' => 'col-md-11', 'placeholder' => 'Buscar...']) !!}
      <span class="input-group-btn" >
        <button class="btn btn-info" type="submit"><li class="fa fa-search"></li></button>
      </span>
    </div><!-- /input-group -->
{!! Form::close() !!}
  </div><!-- /.col-lg-6 -->
</div>
        <button class="navbar-toggler navbar-toggler-right text-uppercase bg-primary text-white rounded" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          Menu
          <i class="fa fa-bars"></i>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">

            <li class="nav-item mx-0 mx-lg-1">
              <a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" href="#portfolio">Seccion</a>
            </li>
            <li class="nav-item mx-0 mx-lg-1">
              <a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" href="#about">Sobre nosotro</a>
            </li>
            <li class="nav-item mx-0 mx-lg-1">
              <a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" href="#contact">Contactos</a>
            </li>

                        <li class="nav-item mx-0 mx-lg-1">
                                      @if (Auth::check())

                            <li class="dropdown nav-item mx-0 mx-lg-1">
                                <a href="#" class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" data-toggle="dropdown" role="button" aria-expanded="false">
                                    {{ Auth::user()->name }}
                                </a>

                                <ul class="dropdown-menu" role="menu">
                                    <li>
                                        <a href="{{ route('logout') }}">
                                            Logout
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        @else
              <a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" href="{{ route('login-get') }}" title="Login">ENTRAR</a>
                        @endif
            </li>
                                                <li class="nav-item mx-0 mx-lg-1">
              <a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger fa fa-shopping-cart" href="{{ route('cart-show') }}" style="font-size: 24px;"> {{count($cart)}}</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  <link rel="stylesheet" href="{{ asset('css/mai.css') }}">
</body>
