       @include('libreri')
         <body id="page-top">


    <nav class="navbar navbar-expand-lg bg-secondary fixed-top text-uppercase" id="mainNav">
<span style="font-size:30px;cursor:pointer" onclick="openNav()">&#9776;</span>
        <a class="navbar-brand js-scroll-trigger" href="/Tiendita">CO-Tienda virtual</a>
      <div class="container">
                <div class="row col-md-5">
<div class="col-md-12">
{!! Form::open(['route' => 'seccione', 'method' => 'GET', 'role' => 'search']) !!}
    <div class="input-group">
      {!! Form::text('name', null, ['class' => 'col-md-11', 'placeholder' => 'Buscar...']) !!}
      <span class="input-group-btn" >
        <button class="btn btn-info" type="submit"><li class="fa fa-search"></li></button>
      </span>
    </div><!-- /input-group -->
{!! Form::close() !!}
  </div><!-- /.col-lg-6 -->
</div>
        <button class="navbar-toggler navbar-toggler-right text-uppercase bg-primary text-white rounded" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          Menu
          <i class="fa fa-bars"></i>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
                            <li class="dropdown nav-item mx-0 mx-lg-1">
                                <a href="#" class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" data-toggle="dropdown" role="button" aria-expanded="false">
Categorias
                                </a>

                                <ul class="dropdown-menu" role="menu">
                                    <li>
              @foreach($cate as $categor)
                      <a href="{{ route('products-cate', $categor->id) }}">{{$categor->name}}</a>
              @endforeach
                                    </li>
                                </ul>
                            </li>
            <li class="nav-item mx-0 mx-lg-1">
              <a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" href="#portfolio">Seccion</a>
            </li>
            <li class="nav-item mx-0 mx-lg-1">
              <a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" href="#about">Sobre nosotro</a>
            </li>
            <li class="nav-item mx-0 mx-lg-1">
              <a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" href="#contact">Contactos</a>
            </li>

                        <li class="nav-item mx-0 mx-lg-1">
                                      @if (Auth::check())

                            <li class="dropdown nav-item mx-0 mx-lg-1">
                                <a href="#" class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" data-toggle="dropdown" role="button" aria-expanded="false">
                                    {{ Auth::user()->name }}
                                </a>

                                <ul class="dropdown-menu" role="menu">
                                    <li>
                                        <a href="{{ route('logout') }}">
                                            Logout
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        @else
              <a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" href="{{ route('login-get') }}" title="Login">ENTRAR</a>
                        @endif
            </li>
                                                <li class="nav-item mx-0 mx-lg-1">
              <a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger fa fa-shopping-cart" href="{{ route('cart-show') }}" style="font-size: 24px;"> {{count($cart)}}</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  <link rel="stylesheet" href="{{ asset('css/mai.css') }}">
</body>
            @extends('store.partials.foot')
          <div id="mySidenav" class="sidenav">
  <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
  <a href="#">About</a>
  <a href="#">Services</a>
  <a href="#">Clients</a>
  <a href="#">Contact</a>
</div>


<style>
body {
    font-family: "Lato", sans-serif;
}

.sidenav {
    height: auto;
    width: 200;
    position: absolute;
    z-index: 1;
    top: 80;
    left: 0;
    background-color: white;
    overflow-x: hidden;
    transition: 0.5s;
    padding-top: 60px;
}

.sidenav a {
    padding: 8px 8px 8px 32px;
    text-decoration: none;
    font-size: 15px;
    color: #818181;
    display: block;
    transition: 0.3s;
}

.sidenav a:hover {
    color: black;
}

.sidenav .closebtn {
    position: absolute;
    top: 0;
    right: 25px;
    font-size: 36px;
    margin-left: 50px;
}

#portfolio {
    transition: margin-left .5s;
    padding: 60px;
    margin-left: 200px;
}

@media screen and (max-height: 450px) {
  .sidenav {padding-top: 15px;}
  .sidenav a {font-size: 18px;}
}
</style>
<script>
function openNav() {
    document.getElementById("mySidenav").style.width = "200px";
    document.getElementById("portfolio").style.marginLeft = "200px";
}

function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
    document.getElementById("portfolio").style.marginLeft= "0";
}
</script>