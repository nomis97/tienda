      <!-- Bootstrap core CSS -->
        {!!Html::style('css/app.css')!!}
        {!!Html::style('css/bootstrap.min.css')!!}
        {!!Html::style('css/font-awesome.css')!!}
        {!!Html::style('css/font-awesome.min.css')!!}


    <!-- Custom fonts for this template -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css">

    <!-- Plugin CSS -->
            {!!Html::style('css//magnific-popup.css')!!}


    <!-- Custom styles for this template -->
                {!!Html::style('css/freelancer.css')!!}
                
    {!!Html::script('js/bootstrap.bundle.min.js')!!}
    {!!Html::script('js/jquery.min.js')!!}

    <!-- Plugin JavaScript -->

    {!!Html::script('js/jquery.easing.min.js')!!}
    {!!Html::script('js/jquery.magnific-popup.min.js')!!}

    <!-- Contact Form JavaScript -->

        {!!Html::script('js/jqBootstrapValidation.js')!!}
        {!!Html::script('js/contact_me.js')!!}

    <!-- Custom scripts for this template -->
        {!!Html::script('js/freelancer.min.js')!!}

    {!!Html::script('js/app.js')!!}