@extends('admin.template')

@section('content')
    
    <div class="container text-center">
        <div class="page-header">
            <h1>
                <i class="fa fa-shopping-cart"></i>
                PRODUCTOS <small>[Agregar producto]</small>
            </h1>
        </div>

        <div class="row">
            <div class="col-md-offset-3 col-md-6">
                    @if (count($errors) > 0)
                        @include('admin.partials.errors')
                    @endif
                <div class="page">

<form  method="POST"  action="{{ route('admin.product.store') }}" class="formarchivo"   enctype="multipart/form-data">

                 <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>"> 
                                                      <div class="form-group">
                            <label class="control-label" for="category_id">Categoría</label>
<select  name="category_id" class="form-control" id="category_id">
@foreach ($categories as $categorie)
    <option value="{{ $categorie->id }}">{{$categorie->name}}</option>
@endforeach
</select>
                        </div>
        
                        <div class="form-group">
                            <label for="name">Nombre:</label>
                            <input type="text" name="name" class="form-control" autofocus="autofocus" placeholder="Ingresa el nombre">
                        </div>
                        
                        <div class="form-group">
                            <label for="extract">Extracto:</label>
                                            <input type="text" name="extract" id="extract" class="form-control" placeholder="Ingresa el extracto">
                        </div>
                        
                        <div class="form-group">
                            <label for="description">Descripción:</label>
    <textarea type="text" name="description" id="description" class="form-control" placeholder="Ingresa la description"></textarea>
                        </div>
                        
                        <div class="form-group">
                            <label for="price">Precio:</label>
                            <input type="text" name="price" id="price" class="form-control" placeholder="Ingresa el precio">
                        </div>
                        
                        <div class="form-group">
                            <label for="image">Imagen:</label>
                            
<input type="file" name="file" id="file">
                        </div>
                        
                        <div class="form-group">
                            <label for="visible">Visible:</label>
                                
                            <input type="checkbox" name="visible">
                        </div>
                        
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">Guardar</button>
                            <a href="{{ route('admin.product.index') }}" class="btn btn-warning">Cancelar</a>
                        </div>
                    </form>                    
                </div>
                
            </div>
        </div>
        

    </div>

@stop

