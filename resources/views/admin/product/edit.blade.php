@extends('admin.template')

@section('content')
	
	<div class="container text-center">
		<div class="page-header">
			<h1>
				<i class="fa fa-shopping-cart"></i>
				PRODUCTOS <small>[Editar producto]</small>
			</h1>
		</div>

		<div class="row">
            <div class="col-md-offset-3 col-md-6">
                
                <div class="page">
                    
                    @if (count($errors) > 0)
                        @include('admin.partials.errors')
                    @endif
                    
                    <form  method="POST"  action="{{ route('admin.product.update') }}" class="formarchivo"   enctype="multipart/form-data">

                        <input type="hidden" name="_method" value="PUT">
                                     <input type="hidden" name="id_pro" value="<?= $product->id; ?>"> 

                 <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>"> 
                                                      <div class="form-group">
                            <label class="control-label" for="category_id">Categoría</label>
<select  name="category_id" class="form-control" id="category_id">
    <option value="{{ $product->category_id }}">{{$product->category->name}}</option>
</select>
                        </div>
        
        
                        <div class="form-group">
                            <label for="name">Nombre:</label>
                            <input type="text" name="name" class="form-control" autofocus="autofocus" placeholder="Ingresa el nombre" value="{{$product->name}}">
                        </div>
                        
                        <div class="form-group">
                            <label for="extract">Extracto:</label>
                                            <input type="text" name="extract" id="extract" class="form-control" placeholder="Ingresa el extracto" value="{{$product->extract}}">
                        </div>
                        
                        <div class="form-group">
                            <label for="description">Descripción:</label>
<textarea type="text" name="description" id="description" class="form-control" placeholder="Ingresa la description">{{$product->description}}</textarea>
                        </div>
                        
                        <div class="form-group">
                            <label for="price">Precio:</label>
                            <input type="text" name="price" id="price" class="form-control" placeholder="Ingresa el precio" value="{{$product->price}}">
                        </div>
                        
                        <div class="form-group">
                            <label for="image">Imagen:</label>
                            
<input type="file" name="file" id="file">
                        </div>
                        
                        <div class="form-group">
                            <label for="visible">Visible:</label>
                            <input type="checkbox" name="visible" {{ $product->visible == 1 ? "checked='checked'" : '' }}>
                        </div>
                
                        
                        
                        
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">Guardar</button>
                            <a href="{{ route('admin.product.index') }}" class="btn btn-warning">Cancelar</a>
                        </div>
                    
</form>                    
                </div>
                
            </div>
        </div>
        

	</div>

@stop