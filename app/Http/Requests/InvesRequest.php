<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use Redirect;

class InvesRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
      * @param  array  $errors
     * @return \Symfony\Component\HttpFoundation\Response
     */

   

    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
                        'titulo' => 'required',
                        'integrantes' => 'required',
                        'apellidos',
                        'descripcion'=>   'required',
                        'ciudad' => 'Alpha',
                        'institucion' => 'Alpha',
                        'ocupacion' => 'Alpha',
                        'tipousuario',
              ];
    }


       public function messages()
    {
        return [
            
            'titulo.required' =>  'Ingresar Nombres es obligatorio',
                         'integrantes.required' =>  'Ingresar Nombres es obligatorio',
                         'apellidos.required' =>  'Ingresar Apellidos es obligatorio',
                         'descripcion.required' =>  'el descripcion es un campo obligatorio',
                         'ciudad.required' =>  'Ingresar una ciudad es obligatorio',
                         'ciudad.alpha' =>  'la ciudad no puede contener numeros en su nombre',
                         'email.required' =>  'Ingresar un email es obligatorio',
                         'email.email' =>  'el email debe tener un formato valido',
                         'institucion.required' =>  'Ingresar una institucion es obligatorio',
                         'ocupacion.required' =>  'Ingresar la ocupacion es obligatorio',
                         'tipousuario.numeric' =>  'Ingresar un tipo de usuario valido ides entre 1 y 2',
          
               ];
    }



     /**
     * Get the proper failed validation response for the request.
     *
     * @param  array  $errors
     * @return \Symfony\Component\HttpFoundation\Response
     
    public function response(array $errors)
    {
        
        return Redirect::to("/mostrar_errores")
                                        ->withInput($this->except($this->dontFlash))
                                        ->withErrors($errors, $this->errorBag);
    }
    */







}
