<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Product;
use App\category;

class StoreController extends Controller
{
    public function index()
    {
    	$products = Product::all();
                $product = Product::all();
        $cart = \Session::get('cart');

                $cate = Category::get();

    	//dd($products);
    	return view('store.index', compact('products', 'cate', 'product', 'cart'));
    }
        public function seccione(Request $request)
    {
        $products = Product::name($request->get('name'))->get();
                $cate = Category::get();
                        $cart = \Session::get('cart');

        //dd($products);
        return view('store.partials.hogar', compact('products', 'cate', 'cart'));
    }
            public function seccion($category_id)
    {
        $products = Product::where('category_id', $category_id)->get();
                $cate = Category::get();
                        $cart = \Session::get('cart');

        //dd($products);
        return view('store.partials.hogar', compact('products', 'cate', 'cart'));
    }
    public function show($slug)
    {
    	$product = Product::where('slug', $slug)->first();
    	//dd($product);
                        $cart = \Session::get('cart');

    	return view('store.show', compact('product', 'cart'));
    }
}
