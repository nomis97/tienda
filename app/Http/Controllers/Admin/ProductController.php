<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\SaveProductRequest;
use App\Http\Controllers\Controller;
use Storage;
use Illuminate\Http\JsonResponse;

use App\Product;
use App\Category;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $products = Product::orderBy('id', 'desc')->paginate(5);
        //dd($products);
        return view('admin.product.index', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $categories = Category::all();
        //dd($categories);
        return view('admin.product.create')->with('categories',$categories);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(SaveProductRequest $request)
    {

            $ima = $request->file('file');

                          $producto= new Product; 
             $producto->name= $request->input("name");
             $producto->slug= str_slug($request->input('name'));
             $producto->description= $request->input("description");
             $producto->extract= $request->input("extract");
             $producto->price= $request->input("price");
             $producto->visible= $request->has('visible') ? 1 : 0;
             $producto->category_id= $request->input("category_id");

           $carpeta=$request->input("category_id");
           $im=$carpeta."/".$request->input("visible")."_".$ima->getClientOriginalName();
           $r1=Storage::disk('fotografias')->put($im,  \File::get($ima) );
           $rutai="storage/fotografias/".$im;
           $producto->image=$rutai;
             $producto->save();

        $message = $producto ? 'Producto agregado correctamente!' : 'El producto NO pudo agregarse!';
        
        return redirect()->route('admin.product.index')->with('message', $message);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show(Product $product)
    {
        return $product;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit(product $product)
    {

        //dd($categories);
        return view('admin.product.edit')->with('product',$product);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(SaveProductRequest $request, Product $product)
    {
                    $ima = $request->file('file');
   $data=$request->all();
        
        $idUsuario=$data["id_pro"];
             $producto= Product::find($idUsuario);
             $producto->name= $request->input("name");
             $producto->slug= str_slug($request->input('name'));
             $producto->description= $request->input("description");
             $producto->extract= $request->input("extract");
             $producto->price= $request->input("price");
             $producto->visible= $request->has('visible') ? 1 : 0;
             $producto->category_id= $request->input("category_id");

                   $carpeta=$request->input("category_id");
           $im=$carpeta."/".$request->input("visible")."_".$ima->getClientOriginalName();
           $r1=Storage::disk('fotografias')->put($im,  \File::get($ima) );
           $rutai="storage/fotografias/".$im;
           $producto->image=$rutai;

        $updated = $producto->save();
        
        $message = $updated ? 'Producto actualizado correctamente!' : 'El Producto NO pudo actualizarse!';
        
        return redirect()->route('admin.product.index')->with('message', $message);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy(Product $product)
    {
        $deleted = $product->delete();
        
        $message = $deleted ? 'Producto eliminado correctamente!' : 'El producto NO pudo eliminarse!';
        
        return redirect()->route('admin.product.index')->with('message', $message);
    }
}
